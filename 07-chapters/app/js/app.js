var base_domain = 'http://37.218.246.155';
var base_url = base_domain + '/warpweft-wiki/'; // use trailing slash plz
var apiurl = base_url + 'api.php';

var prop_exclusions = [
  "TOC", "Person", "Condition", "Herengracht_401", "Place",
  "Material", "Date", "Author", "Theme", "Provenance",
  "Language", "Pattern", "Revision"
];

var starting_chapter = 'How I Came to Her Closet';

// set up vue-router
const routes = [
  { path: '/notes',       component: narrativeComponent },
  { path: '/tapestry',    component: tapestryComponent },
  { path: '/epistolary',  component: correspondenceComponent },
  { path: '/colophon',    component: colophonComponent },
  { path: '/foreword',    component: forewordComponent },
  { path: '/',            redirect: '/notes' },
];
const router = new VueRouter({ routes });

var vm = new Vue({
  el: '#app',
  router: router,
  components: {
    'narrative': narrativeComponent,
    'tapestry': tapestryComponent, 
    'correspondence': correspondenceComponent,
    'colophon': colophonComponent,
    'foreword': forewordComponent,
  },
  name: 'chapters',
  data: {
    debug: false,
  },

  ready: function() {
    $(this.$el).foundation();
  },
});
