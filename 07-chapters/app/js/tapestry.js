// this.detail.imagelinks = iuData.filter(page => !page.title.startsWith('File:'));
var initialProperty = 'Recurring Garment';
var initialValue = 'Studio Smock';
var propertiesToIgnore = ['File', 'Page_has_default_form', 'Properties', 'Property', 'Category', 'Creature', 'Location'];
var propertiesToInclude = ['TOC', 'Date', 'Theme', 'Author', 'Treasured Object', 'Recurring Garment', 'Technique'];

function title_compare(a,b) {
  // https://stackoverflow.com/a/1129270/122400
  if (a.title < b.title)
    return -1;
  if (a.title > b.title)
    return 1;
  return 0;
}

var tapestryComponent = {
  template: '#tapestry-template',
  data: function() {
    return {
      status: '',
      results: [],
      pages: [],
      properties: [],
      activeValues: [],
      activeProperty: 'Loading',
      selectedProperty: '',
      selectedValue: '',
      detail: {
        title: '',
        imageURL: '',
        content: '',
        preview: '',
        properties: [],
        backlinks: [],
        imagelinks: []
      }
    };
  },
  
  props: ['debug'],

  ready: function() {
    $(this.$el).foundation();
  },

  mounted: function() {
    // stop video when closing sidebar
    $(document).on(
      'closed.zf.reveal', '[data-reveal]', function () {
        if ($('#detail-video').length) {
          $('#detail-video')[0].pause();
          console.log('video closed');
        }
      }
    );

    // init vars
    this.selectedPage = null;
    this.nextPage = null;
    this.previousPage = null;
    this.getAllProperties();

    if (this.$route.query.value) {
      this.selectProperty(this.$route.query.prop);
      this.selectValue(this.$route.query.prop, this.$route.query.value);
    } else if (this.$route.query.prop) {
      this.selectProperty(this.$route.query.prop);
    } else {
      this.selectProperty(initialProperty);
      this.selectValue(initialProperty, initialValue);
    }
    console.log('tapestry mounted');
  },

  methods: {
    selectValue: function(prop, value, offset) {
      this.selectedProperty = prop;
      this.selectedValue = value;
      this.activeProperty = prop;

      this.status = 'Loading images...';
      if (this.selectedProperty != prop) {
        this.selectProperty(prop);
      }
      if (!offset) {
        this.results = [];
        this.pages = [];
        offset = 0;
      }

      var qurl = build_url({
        action: 'askargs', 
        conditions: prop + '::' + value,
        parameters: 'offset=' + offset
      });

      this.$http.get(qurl).then(function (response) {
        var r = JSON.parse(response.bodyText);
        var results = r.query.results; 

        var tqurl = build_url({ 
          action: 'query', 
          titles: Object.getOwnPropertyNames(results).join('|'),
          prop: 'imageinfo',
          iiprop: 'url',
          iiurlheight: 180,
          iilimit: 500,
        });
        this.$http.get(tqurl).then(function (response) {

          if (prop != this.selectedProperty || value != this.selectedValue) {
            // other value selected in the meantime, don't process
            return;
          }

          var thumbresults = JSON.parse(response.bodyText).query.pages; 
          var pageindexes = {};
          for (let t in thumbresults) {
            var o = {
              pageid: thumbresults[t].pageid,
              title: thumbresults[t].title,
            };
            if (o.title.endsWith('mov') || o.title.endsWith('mp4') || o.title.endsWith('ogv') || o.title.endsWith('webm')) {
              o.isVideo = true;
            } else {
              o.isVideo = false;
            }
            if (thumbresults[t].imageinfo) {
              o.thumburl = thumbresults[t].imageinfo[0].thumburl;
              this.results.push(o);
            } else if (!o.title.startsWith('File:')) {
              // thumb not found, it's a page (unless it starts with File:, in which case
              // it's a media file without a thumbnail
              pageindexes[t] = this.pages.length;
              this.pages.push(o);
              var pageurl = build_url({ 
                format: 'json', 
                action: 'parse', 
                page: o.title,
              });
              this.$http.get(pageurl).then(function (response) {
                var detailData = JSON.parse(response.bodyText).parse;
                var content = get_page_html(detailData);
                this.pages[pageindexes[t]].content = content;
                this.pages[pageindexes[t]].preview = content.substring(0,150);
                this.$forceUpdate();
              });
            } else {
              // no imageinfo property -- Mediawiki limitation
              // we do a request for this single page to get the property
              var turl = build_url({ 
                action: 'query', 
                titles: o.title,
                prop: 'imageinfo',
                iiprop: 'url',
                iiurlheight: 180,
                iilimit: 500,
              });
              this.$http.get(turl).then(function (response) {
                var pages = JSON.parse(response.bodyText).query.pages; 
                var result = pages[Object.keys(pages)[0]];
                if (!result.imageinfo) {
                  console.log('WARN: Media file without thumbnail: ' + o.title);
                }
                console.log(result);
                var obj = {
                  pageid: result.pageid,
                  title: result.title,
                  thumburl: result.imageinfo[0].thumburl
                };
                if (obj.title.endsWith('mov') || obj.title.endsWith('mp4') || obj.title.endsWith('ogv') || obj.title.endsWith('webm')) {
                  obj.isVideo = true;
                } else {
                  obj.isVideo = false;
                }
                this.results.push(obj);
              });
            }
          }
          if (r['query-continue-offset']) {
            // there are more results, repeat query with indicated offset
            this.selectValue(prop, value, r['query-continue-offset']);
          } else {
            this.status = 'Found ' + this.results.length + ' images and ' + this.pages.length + ' pages.';
            $('#sidebar-reveal').foundation();
            console.log('foundation reloaded for sidebar');
          }
        });
      }, function (response) {
        this.status = 'Error ' + response.status + ' while fetching images.';
        // error callback
      });
    },

    selectNextPage: function() {
      this.selectPage(this.nextPage);
    },
    selectPreviousPage: function() {
      this.selectPage(this.previousPage);
    },

    selectPage: function(title, ev) {
      this.selectedPage = title;
      this.detail = {
        title: '',
        imageURL: '',
        content: '',
        preview: '',
        properties: [],
        backlinks: [],
        imagelinks: [],
        isRestricted: false,
        isVideo: false,
      };

      var result = this.results.filter(r => r.title === title)[0];
      var idx = this.results.indexOf(result);
      var resultArray;
      if (title.startsWith('File:')) {
        resultArray = this.results;
      } else {
        resultArray = this.pages;
      }
      this.nextPage = idx < resultArray.length - 1 ? resultArray[idx + 1].title : resultArray[0].title;
      this.previousPage = idx > 0 ? resultArray[idx - 1].title : resultArray[resultArray.length - 1].title;

      if (ev) {
        $('.quilt-image').parent().removeClass('active-image');
        $('.quilt-video').parent().removeClass('active-image');
        ev.target.classList.add('active-image');
      }

      var txturl = build_url({ 
        action: 'parse', 
        page: title,
      });
      this.$http.get(txturl).then(function (response) {
        var detailData = JSON.parse(response.bodyText).parse;
        this.detail.title = detailData.title;
        this.detail.id = detailData.pageid;
        var html = get_page_html(detailData);
        // remove all <li> elements
        var newhtml = '';
        $.each($.parseHTML(html), function(i, el) {
          if (el.tagName && el.tagName != 'LI') {
            newhtml += el.outerHTML;
          }
        });
        if (!newhtml) {
          // if we didn't get anything, it means the content is
          // plaintext. Copy it as is
          newhtml = html;
        }
        this.detail.content = newhtml;
        this.detail.preview = this.detail.content.substring(0,150);
      });

      if (title.startsWith('File:')) {
        // image url
        var iiurl = build_url({ 
          action: 'query', 
          titles: title,
          prop: 'imageinfo',
          iiprop: 'url',
        });
        this.$http.get(iiurl).then(function (response) {
          var pages = JSON.parse(response.bodyText).query.pages;
          var imgurl = pages[Object.keys(pages)[0]].imageinfo[0].url;
          if (imgurl.endsWith('mov') || imgurl.endsWith('webm') || imgurl.endsWith('ogv') || imgurl.endsWith('mp4')) {
            this.detail.videoURL = imgurl;
          } else {
            this.detail.imageURL = imgurl;
          }
        });
        // imageusage
        var iuurl = build_url({ 
          action: 'query', 
          list: 'imageusage',
          iutitle: title,
        });
        this.$http.get(iuurl).then(function (response) {
          var iuData = JSON.parse(response.bodyText).query.imageusage;
          this.detail.imagelinks = iuData;
        });
      }

      // backlinks
      var blurl = build_url({ 
        action: 'query', 
        list: 'backlinks',
        bltitle: title,
      });
      this.$http.get(blurl).then(function (response) {
        var blData = JSON.parse(response.bodyText);
      });

      // properties
      var prurl = build_url({ 
        action: 'browsebysubject', 
        subject: title,
      });
      this.$http.get(prurl).then(function (response) {
        var prData = JSON.parse(response.bodyText).query.data;
        var values = [];
        $.each(prData, function(idx, prop){
          if (!prop.property.startsWith('_')) {
            $.each(prop.dataitem, function(i, value) {
              values.push({property: prop.property, value: value.item.replace('#0#', '')});
            });
          }
        });
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/some
        if (values.some(item => item.property == 'Herengracht_401' && (item.value == 'The_Attic' || item.value == 'The_Photo_Archive'))) {
          this.detail.isRestricted = true;
        }
        if (title.endsWith('mov') || title.endsWith('mp4') || title.endsWith('ogv') || title.endsWith('webm')) {
          this.detail.isVideo = true;
        }
        this.detail.properties = values;
      });
    },

    getAllProperties: function() {
      var propurl = build_url({ 
        action: 'browsebyproperty', 
      });
      this.$http.get(propurl).then(function (response) {
        var propInfo = JSON.parse(response.bodyText).query;
        var userProps = {};

        allProps = Object.keys(propInfo);
        // join the allProps and propertiesToInclude arrays
        allProps.push.apply(allProps, propertiesToInclude);
        for (let idx = 0; idx < allProps.length; idx++) {
          var prop = allProps[idx];
          if ((propInfo[prop] && 'isUserDefined' in propInfo[prop] && propertiesToIgnore.indexOf(prop) === -1) || 
              propertiesToInclude.indexOf(prop) != -1) {
            // add property to dictionary
            userProps[prop] = [];

            // Load all values for this property
            // http://smw.referata.com/wiki/List_the_set_of_unique_values_for_a_property
            // e.g. [[Person::+]]|mainlabel=-|headers=hide|?Person|limit=10000
            var valurl = build_url({ 
              action: 'ask', 
              query: '[[' + prop + '::+]]|mainlabel=-|headers=hide|?' + prop + '|limit=10000'
            });
            this.$http.get(valurl).then(function (response) {
              // create array of unique values
              // clunky but works[tm]
              var thisprop = allProps[idx];
              var r = JSON.parse(response.bodyText);
              var results = r.query.results;
              for (var result in results) {
                var pageValues = results[result].printouts[thisprop.replace('_', ' ')];
                for (var i in pageValues) {
                  var valueInfo = pageValues[i];
                  var val;
                  if (thisprop === 'Date') {
                    val = valueInfo.raw.replace('1/', '');
                  } else {
                    val = valueInfo.fulltext;
                  }
                  if (userProps[thisprop].indexOf(val) === -1) {
                    userProps[thisprop].push(val);
                  }
                }
              }
              userProps[thisprop] = userProps[thisprop].sort();
            });
          }
        }
        this.properties = userProps;
      });
    },

    selectProperty: function(prop) {
      this.activeProperty = prop;
    },
    prevProperty: function() {
      var props = Object.keys(this.properties); 
      var idx = props.indexOf(this.activeProperty);
      if (idx === 0) {
        idx = props.length;
      }
      var prevProp = props[idx-1];
      this.selectProperty(prevProp);
    },
    nextProperty: function() {
      var props = Object.keys(this.properties); 
      var idx = props.indexOf(this.activeProperty);
      if (idx >= props.length-1) {
        idx = -1; // will be added to in next line
      }
      var nextProp = props[idx+1];
      this.selectProperty(nextProp);
    },
    

    getValueListClass: function(v) {
      // used for selected item class in value list
      if (v === this.selectedValue) {
        return 'selected-value';
      }
    }
  },


};
