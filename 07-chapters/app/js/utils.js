var base_domain = 'http://37.218.246.155';

function fix_image_urls(img) {
  var src;
  if (window.location.href.indexOf('localhost') != -1) {
    console.log("it's localhost");
    src = base_domain + img.src.replace(/http:\/\/localhost:8000/, '');
  } else if (window.location.href.indexOf('koizo') != -1) {
    console.log("it's koizo");
    src = base_domain + img.src.replace(/http:\/\/koizo.org/, '');
  } else if (window.location.href.indexOf('~manufactura') != -1) {
    console.log("it's warpweft");
    src = img.src;
  } else if (window.location.href.indexOf('warpweftmemory.net') != -1) {
    console.log("it's warpweft live");
    src = base_domain + img.src.replace(/http:\/\/warpweftmemory.net/, '').replace(/http:\/\/www.warpweftmemory.net/, '');
  } else {
    console.log("server not identified!");
  }
  img.src = src;
  img.srcset = img.srcset.replace(/\/warpweft-wiki/g, base_domain + '/warpweft-wiki');
}


function build_url(querydict) {
  // helper function to construct the URL for the API query
  // var qurl = build_url({ format: 'json', action: 'query', 'titles': 'Property:Person' });
  if ( !querydict ) { return apiurl; }
  var url = apiurl + '?';
  // add 'origin' parameter: https://www.mediawiki.org/wiki/API:Cross-site_requests
  // url += 'origin=' + encodeURIComponent('*'); 
  // url += 'origin=' + encodeURIComponent('http://localhost:9000'); 
  url += '&format=json';
  // url += '&callback=?';
  $.each(querydict, function(key, value) {
    if ( url.substr(-1) != '?') {
      url += '&';
    }
    url += key + '=' + encodeURIComponent(value); 
  });
  return url;
}

function get_page_html(detailData, outer) {
  var text = detailData.text['*'];
  text = text.replace(/\n|\r/g, '')
    .replace(/<p><br \/><\/p>/ig, '');
  var $nodes = $.parseHTML(text);
  var html = '';
  $.each($nodes, function(i, el) {
    // omit div.properties
    if (el.className != 'properties') {
      if (el.innerHTML || el.tagName == "HR") {
        if ($(el).find('img.restricted').length) {
          $(el).find('img.restricted').parent().parent().addClass('restricted');
        }

        if ($(el).find('img').length) {
          $.each($(el).find('img'), function(idx, img) {
            var parentTag = $(img).parent();
            if ($(parentTag[0].tagName == 'A')) {
              // place media file outside the hyperlink and remove the link
              $(parentTag).parent().append(img);
              $(parentTag).remove();
            }
          });

          // turn wiki links into plaintext
          $.each($(el).find('a[href^="/warpweft-wiki"]'), function(idx, link) {
            // console.log(link);
            $(link).contents().unwrap();
          });

          $(el).find('img').attr('src', function(i, val) {
            // add base domain to image src attribute
            return base_domain + val;
          });
          el = $(el)[0];
        }

        if (outer) {
          html += el.outerHTML;
        } else {
          html += el.innerHTML;
        }
      }
    }
  });
  return html;
}

function get_chapter_elements(detailData) {

  var chapter = {
    pageid: detailData.pageid,
    title: detailData.title.replace(/_/g, ' '),
    paragraphs: [],
    properties: [],
    images: [],
    headerIllustration: null,
    imageprop1: null,
    imageprop2: null,
    imageprop1count: null,
    imageprop2count: null,
    refs: []
  };

  var text = detailData.text['*'];
  text = text.replace(/\n|\r/g, '')
    .replace(/<p><br \/><\/p>/ig, '');
  var $nodes = $.parseHTML(text);
  $.each($nodes, function(i, el) {
    // omit div.properties
    if (el.className != 'properties') {
      if (el.innerHTML) {
        if (!$(el).find('p').length && $(el).find('img').length) {
          // case for headers, which are not inside paragraphs
          var img = $(el).find('img').get(0);
          fix_image_urls(img);
          var img_html = img.outerHTML;
          chapter.paragraphs.push(img_html);
        }
        $.each($(el).find('p, blockquote'), function(pi, para) {
          $.each($(para).find('a'), function(ai, link) {
            var title = link.href.split('/').reverse()[0];
            if (!title.startsWith('File:')) {
              if (link.hash.includes('cite_note')) {
                link.hash = link.hash.replace('cite_note', 'cite_note-' + chapter.pageid);
              } else {
                // text paragraph, change links to Vue method calls
                $(link).attr('onclick', 'vm.$refs.current.loadChapter(\'' + title + '\', event)');
                $(link).addClass('chapterlink');
                $(link).attr('href', '#');
              }
            } else {
              // place media file outside the hyperlink and remove the link
              $(link).parent().append($(link).find('img'));
              $(link).remove();
            }
          });
          $.each($(para).find('sup'), function(ai, sup) {
            // fix reference URLs
            sup.id = sup.id.replace(/cite_ref/g, 'cite_ref-' + chapter.pageid);
          });
        });
        $.each($(el).find('ol.references'), function(pi, ol) {
          $refs = $(ol).find('li');
          $.each($refs, function(refidx, ref) {
            // move arrow link to the end
            $(ref).find('.mw-cite-backlink').insertAfter($(ref).find('.reference-text'));
            // make links open in new tab
            $(ref).find('a').not('[href^="#cite_ref"]').attr('target', '_blank');
            // fix ref links and place its HTML in the chapter data
            refHTML = ref.innerHTML.replace(/cite_note/g, 'cite_note-' + chapter.pageid)
                                   .replace(/cite_ref/g, 'cite_ref-' + chapter.pageid);
            refHTML = '<span id=' + ref.id.replace(/cite_note/g, 'cite_note-' + chapter.pageid) + '>' + refHTML + '</span>';
            chapter.refs.push(refHTML);
          });
        });
        $.each($(el).find('p, blockquote'), function(pi, para) {
          // fix image urls
          $.each($(para).find('img'), function(ai, img) {
            fix_image_urls(img);
          });
          // remove video attributes set by MediaWiki
          $.each($(para).find('video'), function(ai, vid) {
            $(vid).removeAttr('style').removeAttr('width').removeAttr('height');
          });
          var paracontent = para.innerHTML;
          if (para.tagName == 'BLOCKQUOTE') {
            paracontent = '<span class="quote">' + paracontent + '</span>';
          } else {
            paracontent = paracontent.replace(/^<br>/ig, '');
          }
          if (!(para.tagName == 'P' && para.parentElement.tagName == 'BLOCKQUOTE')) {
            // this check ensures we don't duplicate blockquote content,
            // since blockquotes have <p> elements inside
            chapter.paragraphs.push(paracontent);
          }
        });
      }
    }
    if (el.className == 'image-properties') {
      var props = $(el).find('li');
      if (props.length == 1) {
        chapter.imageprop1 = props[0].innerHTML;
      } else if (props.length == 2) {
        chapter.imageprop1 = props[0].innerHTML;
        chapter.imageprop2 = props[1].innerHTML;
      }
    }
  });

  // detect header illustration
  if (chapter.paragraphs[0].indexOf('Cd-') != -1) {
    // assign it to chapter object
    chapter.headerIllustration = chapter.paragraphs[0];
    // remove the image's paragraph
    chapter.paragraphs.shift(0);
  }
  return chapter;
}
